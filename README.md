## Системы
Windows, Mac, Linux

## ПО
Java 11.0.7, Apache Maven 3.6.1

## Построение
```bash
mvn install
```

## Разработчик
Кондратенко Ю.Г. (kondratenko_yg@nlmk.com)

## Запуск
```bash
java -jar ./task-manager.jar
```